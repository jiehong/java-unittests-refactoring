package com.jiehong.kata.refactoring.solution1a;

public interface DataHandling {
  boolean exists(final String path);

  String readString(final String path);
}
