package com.jiehong.kata.refactoring.solution1a;

/**
 * This is a variant of solution 1, trying to be smarter in keeping the exact same method signature.
 *
 * However, here the non-static setter for a static variable is pretty crazy.
 * It's usage implies that `handler` is implicitly null, and that you have to call `setDataHandling`
 * before being able to use `readOrDefault`.
 *
 * This order also is implicit.
 *
 * All of this makes this solution brittle, non-obvious and not thread-safe anyway.
 *
 * Don't implement that.
 */
public final class BaseRefactored {
  private static DataHandling handler;

  public void setDataHandling(final DataHandling handler) {
    BaseRefactored.handler = handler;
  }

  public static String readOrDefault(final String path) {
    if (BaseRefactored.handler.exists(path)) {
      return "Default";
    }
    return BaseRefactored.handler.readString(path);
  }
}
