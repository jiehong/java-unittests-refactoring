package com.jiehong.kata.refactoring.solution1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileHandling implements DataHandling {
  @Override
  public boolean exists(String path) {
    return new File(path).exists();
  }

  @Override
  public String readString(String path) {
    try {
      return Files.readString(Path.of(path));
    } catch (IOException e) {
      return "Default";
    }
  }
}
