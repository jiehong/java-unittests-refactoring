package com.jiehong.kata.refactoring.solution1;

/**
 * This is the "idiomatic" Java way to refactor such a method.
 *
 * Note that here the method signature changed: it no longer is static.
 * This allows for its enclosing class to provide the abstracted IO layer.
 *
 * In the unit-tests, you provide a mock for DataHandling, thus controlling the outcome.
 */
public final class BaseRefactored {
  private final DataHandling handler;

  public BaseRefactored(final DataHandling handler) {
    this.handler = handler;
  }

  public String readOrDefault(final String path) {
    if (this.handler.exists(path)) {
      return "Default";
    }
    return this.handler.readString(path);
  }
}
