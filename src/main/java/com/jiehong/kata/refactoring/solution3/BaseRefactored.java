package com.jiehong.kata.refactoring.solution3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * This solution simply is to do nothing: you accept that this whole piece of code simply is non-testable.
 *
 * The caveat is that this contains some business logic (the if branch, and the return in the catch block),
 * so accepting to no test this bit, means that your integration tests will have to tests those cases instead.
 *
 * Integration tests are more expensive, and more brittle. So this isn't recommended.
 */
public class BaseRefactored {
  public static String readOrDefault(final String path) {
    final var file = new File(path);
    if (!file.exists()) {
      return "Default";
    }
    try {
      return Files.readString(Path.of(path));
    } catch (IOException e) {
      return "Default";
    }
  }
}
