package com.jiehong.kata.refactoring.solution2;


/**
 * Following the Single Responsibility Principle (SRP),
 * we realised that checking for existence of data,
 * and reading the content are 2 different things, thus 2 different responsibilities.
 *
 * So we break the interface of solution1 into 2 parts, and directly provide them to the method.
 *
 * Advantage: not every things might need both methods, so we reduce the burden of the implementing classes.
 *
 * This leads to a very functional (as in functional programming) way to do things.
 * Indeed, each interface now only contains 1 single method.
 * A method really is a function, so we can pass lambdas directly if we want to, as shown in `howToUse`.
 */
public final class BaseRefactoredSrp {
  public static String readOrDefault(final String path,
                                     final DataExistenceChecker checker,
                                     final DataReader reader) {
    if (!checker.exists(path)) {
      return "Default";
    }
    return reader.readString(path);
  }

  private void howToUse() {
    BaseRefactoredSrp.readOrDefault("",
        BaseRefactoredSrp::exist,
        path -> "data");
  }

  private static boolean exist(final String path) {
    return false;
  }
}
