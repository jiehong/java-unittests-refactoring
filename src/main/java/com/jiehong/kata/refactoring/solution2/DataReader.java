package com.jiehong.kata.refactoring.solution2;

@FunctionalInterface
public interface DataReader {
  String readString(final String path);
}
