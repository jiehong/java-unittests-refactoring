package com.jiehong.kata.refactoring.solution2;

@FunctionalInterface
public interface DataExistenceChecker {
  boolean exists(final String path);
}
