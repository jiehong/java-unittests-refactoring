package com.jiehong.kata.refactoring;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * This class cannot be tested by a unit-test.
 *
 * Do you understand why?
 *
 * What are the fundamental issues here?
 */
public final class Base {

  public static String readOrDefault(final String path) {
    final var file = new File(path);
    if (!file.exists()) {
      return "Default";
    }
    try {
      return Files.readString(Path.of(path));
    } catch (IOException e) {
      return "Default";
    }
  }
}
